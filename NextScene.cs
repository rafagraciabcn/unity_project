﻿using UnityEngine;
using System.Collections;

public class NextScene : MonoBehaviour
{

    // Use this for initialization
    private bool chest;

    void Awake()
    {
        chest = true;
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (chest)
        {
            Application.LoadLevel("end");
        }
        chest = false;
    }
    private void OnCollisionExit2D(Collision2D col)
    {
        if (!chest)
        {
            chest = true;
        }
    }
}